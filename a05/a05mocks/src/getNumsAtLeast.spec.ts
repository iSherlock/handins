import getNumsAtLeast from './getNumsAtLeast';
//import request from 'supertest';
//import express from 'express';

import axios from 'axios';
jest.mock('axios');
const axiosMock = axios as jest.Mocked<typeof axios>;
/*  TODO: test the getNumsAtLeast function by mocking the axios library

  You only need 2-4 tests
  1 to ensure that the function is calling the correct url
  1-3 to ensure that the filter is working (NOTE: the filter has 2 equiv. classes)

  Mock axios so that it returns controlled, testable values.
  NOTE: there is no service at https://example.com/api/v1/nums
    so you must mock axios for the tests to work.
  See https://gitlab.com/jhilliker/cpsc2350labdemos

  You may not alter the getNumsAtLeast.ts file.
  Submit only this file
*/
describe('getNumsAtLeast', ()=>{
  //const app = express();
  beforeAll(async () => await axiosMock.get('/'));
  beforeEach(() => axiosMock.get.mockReset());
  
  test('calls Correct URL', async () => {
    const expURL = 'https://example.com/api/v1/nums';
    await axiosMock.get(expURL);
    // eslint-disable-next-line @typescript-eslint/unbound-method
    expect(axiosMock.get).toHaveBeenLastCalledWith(expURL);
  })
  
  test('Good response', async() => {
    const respGood = Promise.resolve({
      data: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    });
    axiosMock.get.mockReturnValueOnce(respGood);
  });
  //I am unsure how to test this one.
  test('bad response', async() => {
    const badResp = Promise.resolve({
      
    })
  })
});