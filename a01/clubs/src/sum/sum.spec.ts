import sum from './sum';

describe('sum', () => {
  test('empty', () => {
    expect(sum()).toBe(0);
  });

  ([
    ['empty', [], 0],
    ['single', [7], 7],
    ['4 +ve', [2, 3, 5, 7], 17],
    ['mix +/-', [2, -3, 5, -7], -3],
  ] as [string, number[], number][]).forEach(
    ([name, input, expected], index) => {
      test(`#${index}: ${name}`, () => {
        expect(sum(...input)).toBe(expected);
      });
    }
  );
});
