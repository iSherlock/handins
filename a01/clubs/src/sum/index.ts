import { Application, Request, Response } from 'express';
import { check, validationResult } from 'express-validator';

import sum from './sum';

function fn(app: Application, path: string): void {
  const toNumArray = (v: string[] | string): number[] => {
    if (typeof v === 'string') {
      if (v.startsWith('[')) {
        // nums=[1,2]
        return JSON.parse(v);
      }
      // nums=1+2
      v = v.split(/ |\+/);
    }
    if (Array.isArray(v)) {
      // nums=1&nums=2
      return v.map(v => parseInt(v, 10));
    }
    throw 'barf';
  };

  const validator = [check('nums').customSanitizer(toNumArray)];

  app.get(path, validator, (req: Request, res: Response) => {
    const valErrors = validationResult(req);
    if (valErrors.isEmpty()) {
      const nums = req.query.nums;
      res.status(200).send('' + sum(...nums));
    } else {
      res.status(400).send(valErrors.array());
    }
  });
}

export default async function(
  app: Application,
  path: string
): Promise<Application> {
  return new Promise((resolve /*, reject*/) => {
    fn(app, path);
    resolve(app);
  });
}
