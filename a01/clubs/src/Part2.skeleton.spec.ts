import Club from './Club';
//import { Member, Gender, MemberImpl } from './Member';
import ClubFactory, { GeneralClub } from './Part2_DIP';

describe('A02', () => {
  let club: Club;

  describe('GeneralClub', () => {
    describe('always', () => {
      beforeEach(() => {
        club = new GeneralClub((): true => true);
      });
      test('TODO', () => {
        expect(club.size).toBe(0);
      });
    });
  });

  describe('ClubFactory', () => {
    describe('adultsOnlyClub', () => {
      beforeEach(() => {
        club = ClubFactory.adultsOnlyClub();
      });
      test('TODO', () => {
        expect(club.size).toBe(0);
      });
    });
    describe('allTheSingleLadiesClub', () => {
      beforeEach(() => {
        club = ClubFactory.allTheSingleLadiesClub();
      });
      test('TODO', () => {
        expect(club.size).toBe(0);
      });
    });
    describe('sizeLimitClub', () => {
      test('TODO', () => {
        club = ClubFactory.sizeLimitClub(0);
        expect(club.size).toBe(0);
      });
    });
    describe('noHomerClub', () => {
      beforeEach(() => {
        club = ClubFactory.noHomerClub();
      });
      test('TODO', () => {
        expect(club.size).toBe(0);
      });
    });
    describe('noHomersClub', () => {
      beforeEach(() => {
        club = ClubFactory.noHomersClub();
      });
      test('TODO', () => {
        expect(club.size).toBe(0);
      });
    });
    describe('balancedGendersClub', () => {
      beforeEach(() => {
        club = ClubFactory.balancedGendersClub();
      });
      test('TODO', () => {
        expect(club.size).toBe(0);
      });
    });
  });
});
