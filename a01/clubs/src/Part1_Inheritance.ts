import Club, { Member, Gender } from './Club';

/* A01
  Implement the following classes in this file according to their documention:
    AbstractClub, LimitedSizeClub, NoHomerClub, BalancedGendersClub

  You may not use any of the code from Assignment 2.
  All classes in this module should be exported, and none of them should be a default export.

  You should only need 2 linter exceptions:
    *  for an explicit any when required for overloading
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    *  for an unused variable that was specified by the interface, but not used by the contract
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
*/

/** An abstract club missing only the `canAdd` method.
  Subclasses should override `canAdd`.
  @remarks
  `AbstractClub` is-a `Club`
  The class's constructor (if any) does not accept any parameters.
  The list of members must be kept `private`.
  You may not alter the public interface from `Club`.
  You may not implement the `canAdd` method -- leave it abstract.
  You must *overload* the `contains` method.
  No subclasses may maintain their own list of members.
*/
// TODO:  create AbstractClub class here
export abstract class AbstractClub implements Club{
  
  private memberList: Member[] = [];
  private size: number;
  
  constructor(){
    this.size = 0;
  }
  getSize(): number{
    return this.size;
  }
  
  add(...m: Member[]): boolean{
    
    this.memberList.push(...m);
    this.size++;
    return true;
  }
  
  abstract canAdd(m: Member): boolean;
  
  contains(m: Member): boolean{
    for(let i = 0; i > this.memberList.length; i++){
      if(m.name === this.memberList[i].name && m.age === this.memberList[i].age){
        return false;
      }
    }
    return true;
  }
  
  contains(predicate: (m: Member) => boolean): boolean{
    for(let i = 0; i < this.memberList.length; i++){
      if(predicate(this.memberList[i])){
        return true
      }
    }
    return false;
  }
  
  count(predicate: (m: Member) => boolean): number{
    let count = 0;
    for(let i = 0; i < this.memberList.length; i++){
      if(predicate(this.memberList[i])){
        count ++;
      }
    }
    return count;
  }
  
}
// *** CLUBS ***

/** A club limited to a certain number of members.
  Once this number is reached, no other members are permitted to join.
  @remarks
  `LimitedSizeClub` is-a `AbstractClub`
  The class's constructor accepts a `number` that is the maximum number of club members.
  The `canAdd` method should optionally take a `Member` as its parameter.
*/
// TODO:  create LimitedSizeClub class here
export class LimitedSizeClub extends AbstractClub{
  
  
  readonly maxSize: number;
  
  constructor(max: number){
    super();
    this.maxSize = max;
  }
  
  
  add(...m: Member[]):boolean{
    if(this.canAdd(m)){
      super.add(...m);
      return true;
    }
    return false;
  }
  
  
  canAdd(m: Member): boolean{
    if(this. >= this.maxSize || super.contains(m)){//&& !contains(m)
      return false;
    }
    return true;
  }
}
/** A club that does not allow anyone whos name begins with "Homer" to join the club.
  @remarks
  `NoHomerClub` is-a `AbstractClub`
  The class's constructor (if any) does not accept any parameters.
*/
// TODO:  create NoHomerClub class here
export class NoHomerClub extends AbstractClub{
  //readonly size: number;
  
  constructor(){
    super();
  }
  
  
  add(...m: Member[]):boolean{
    if(this.canAdd(m)){
      super.add(...m);
      return true;
    }
    return false;
  }
  
  
  canAdd(m: Member): boolean{
    if(m.name.contains("homer") || super.contains(m)){
      return false;
    }
    return true;
  }
}
/** A club which keeps the number of male and female members within 1 of each other.
  ie: there can never be more than 1 male more than the number of females, and vice versa.
  @remarks
  `BalancedGendersClub` is-a `AbstractClub`
  The class's constructor (if any) does not accept any parameters.
  Override the `add` method to keep track of the number of men vs number of women in the club.
  Do not try to re-order members that wish to join. They are accepted or rejected in the order in which they apply.
*/
// TODO:  create BalancedGendersClub class here
export class BalancedGendersClub extends AbstractClub{
  //readonly size: number;
  balance: number;
  
  constructor(){
    super();
    this.balance = 0;
  }
  
  
  add(...m: Member[]):boolean{
    if(this.canAdd(m)){
      super.add(...m);
      this.balance+= m[2];
      return true;
    }
    return false;
  }
  
  
  canAdd(m: Member): boolean{
    if((this.balance + m.gender) > 1 || (this.balance + m.gender) < -1 || super.contains(m)){
      return false;
    }
    return true;
  }
}