import express from 'express';

/* eslint-disable @typescript-eslint/no-unused-vars */
import sumApp from './sum/index';
/* eslint-enable @typescript-eslint/no-unused-vars */

const app = express();

sumApp(app, '/sum').then(() => {
  const server = app.listen(process.env.NODE_PORT || 8080);
  server.on('listening', () => {
    console.log(server.address());
  });
});
